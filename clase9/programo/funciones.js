function hola_clase() {
    return "Hola Acamica";
}

console.log(hola_clase());

const mensaje = (texto) => {
    console.log(`El texto enviado por parametro es: ${texto}`);
}

console.log(mensaje("Hola de nuevo Acamica"));